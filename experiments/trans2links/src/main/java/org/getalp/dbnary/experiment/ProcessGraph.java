package org.getalp.dbnary.experiment;


import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.tdb.TDBFactory;
import org.getalp.dbnary.LemonOnt;
import org.getalp.dbnary.VarTransOnt;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.flow.*;
import org.jgrapht.alg.interfaces.MaximumFlowAlgorithm;
import org.jgrapht.ext.*;
import org.jgrapht.graph.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

public class ProcessGraph {
    private static Logger log = LoggerFactory.getLogger(ToolsGraph.class);
    private SimpleDirectedGraph<String,DefaultEdge> graph ;
    private SimpleDirectedGraph<String,DefaultEdge> simplfiedGraph ;

    private int sumTP1 ;
    private int sumFP1 ;
    private int sumFN1 ;
    private int sumTN1 ;
    private int sumTP2 ;
    private int sumFP2 ;
    private int sumFN2 ;
    private int sumTN2 ;
    private int sumTP3 ;
    private int sumFP3 ;
    private int sumFN3 ;
    private int sumTN3 ;

    private double p ; // Sampling : probability to select an edge in the samples
    private int s ; // Sampling : number of samples to make
    //private SimpleGraph<String,DefaultEdge> graphUndirected ;

    public ProcessGraph(String fileTDB,List<String> sourceList, int depth){
        Dataset dataset = TDBFactory.createDataset(fileTDB) ;
        dataset.begin(ReadWrite.READ) ;
        Model g = dataset.getDefaultModel() ;

        // Get connected components statistics
        log.debug("Getting connected components...");
        SimpleDirectedGraph<String,DefaultEdge> completeGraph = getJGraph(g) ;
        ConnectivityInspector<String,DefaultEdge> ci = new ConnectivityInspector<String, DefaultEdge>(completeGraph) ;
        List<Set<String>> components = ci.connectedSets() ;
        log.debug("Number of connected components : "+components.size()) ;
        int maxSize = 0 ;
        for(Set<String> component : components){
            String comp = "" ;
            int size = component.size() ;
            if(size>maxSize){
                maxSize = size ;
            }
            /*for(String v : component){
                comp = comp+v+" " ;
            }*/
            log.debug("Component (size "+size+")"/* : "+comp*/) ;
        }
        log.debug("Biggest connected components has size "+maxSize);

        List<Resource> resources = new ArrayList<>() ;
        for(String s : sourceList){
            resources.add(g.getResource(s)) ;
        }
        Model newGraph = getsubGraph(g,resources,depth) ;
        graph = getJGraph(newGraph) ;
        Set<String> vertices = graph.vertexSet() ;
        Set<DefaultEdge> edges = graph.edgeSet() ;
        log.debug("In the subgraph : "+vertices.size()+" vertices and "+edges.size()+" edges.") ;
        //graphUndirected = getUJGraph(newGraph) ;
        //Set<String> uvertices = graphUndirected.vertexSet() ;
        //Set<DefaultEdge> uedges = graphUndirected.edgeSet() ;
        //log.debug("In the undirected subgraph : "+uvertices.size()+" vertices and "+uedges.size()+" edges.") ;
    }

    public ProcessGraph(String fileTDB){
        sumTP1 = 0 ;
        sumFP1 = 0 ;
        sumFN1 = 0 ;
        sumTN1 = 0 ;
        sumTP2 = 0 ;
        sumFP2 = 0 ;
        sumFN2 = 0 ;
        sumTN2 = 0 ;
        sumTP3 = 0 ;
        sumFP3 = 0 ;
        sumFN3 = 0 ;
        sumTN3 = 0 ;

        log.debug("Reading Model...");
        Dataset dataset = TDBFactory.createDataset(fileTDB) ;
        dataset.begin(ReadWrite.READ) ;
        Model g = dataset.getDefaultModel() ;

        // Get connected components statistics
        graph = getJGraph(g) ;

        Set<String> vertices = graph.vertexSet() ;
        Set<DefaultEdge> edges = graph.edgeSet() ;
        log.debug("In the graph : "+vertices.size()+" vertices, "+edges.size()+" edges.") ;

        simplfiedGraph = ToolsGraph.simplifyGraph(graph) ;
        log.debug("In the simplified graph : "+simplfiedGraph.vertexSet().size()+" vertices, "+simplfiedGraph.edgeSet().size()+" edges.") ;

    }

    public ProcessGraph(String fileTDB, double p, int s){
        sumTP1 = 0 ;
        sumFP1 = 0 ;
        sumFN1 = 0 ;
        sumTN1 = 0 ;
        sumTP2 = 0 ;
        sumFP2 = 0 ;
        sumFN2 = 0 ;
        sumTN2 = 0 ;
        sumTP3 = 0 ;
        sumFP3 = 0 ;
        sumFN3 = 0 ;
        sumTN3 = 0 ;

        this.p = p ;
        this.s = s ;

        log.debug("Reading Model...");
        Dataset dataset = TDBFactory.createDataset(fileTDB) ;
        dataset.begin(ReadWrite.READ) ;
        Model g = dataset.getDefaultModel() ;

        // Get connected components statistics
        graph = getJGraph(g) ;

        Set<String> vertices = graph.vertexSet() ;
        Set<DefaultEdge> edges = graph.edgeSet() ;
        log.debug("In the graph : "+vertices.size()+" vertices, "+edges.size()+" edges.") ;
    }

    private void reset(double p, int s){
        this.p = p ;
        this.s = s ;
        sumTP1 = 0 ;
        sumFP1 = 0 ;
        sumFN1 = 0 ;
        sumTN1 = 0 ;
        sumTP2 = 0 ;
        sumFP2 = 0 ;
        sumFN2 = 0 ;
        sumTN2 = 0 ;
        sumTP3 = 0 ;
        sumFP3 = 0 ;
        sumFN3 = 0 ;
        sumTN3 = 0 ;
    }

    private void reset(){
        sumTP1 = 0 ;
        sumFP1 = 0 ;
        sumFN1 = 0 ;
        sumTN1 = 0 ;
        sumTP2 = 0 ;
        sumFP2 = 0 ;
        sumFN2 = 0 ;
        sumTN2 = 0 ;
        sumTP3 = 0 ;
        sumFP3 = 0 ;
        sumFN3 = 0 ;
        sumTN3 = 0 ;
    }

    public static void main(String args[]) throws FileNotFoundException,IOException {
        SimpleDirectedGraph<String,DefaultEdge> g1 = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;

        log.debug("Starting...") ;

        ProcessGraph pg = new ProcessGraph(args[0]) ;

        String sourceWS1 = "http://kaiko.getalp.org/dbnary/fra/__ws_1_avis__nom__1" ;
        String targetLE1 = "http://kaiko.getalp.org/dbnary/eng/advice__Noun__1" ;
        pg.doubleMaxFlowPaths(sourceWS1,targetLE1,pg.simplfiedGraph,pg.graph) ;

        String sourceWS = "http://kaiko.getalp.org/dbnary/eng/__ws_3_student__Noun__1" ;
        String targetLE = "http://kaiko.getalp.org/dbnary/fra/étudiante__nom__1" ;
        pg.doubleMaxFlowPaths(sourceWS,targetLE,pg.simplfiedGraph,pg.graph) ;

        // STATS OF THE GRAPH : VERTICES, ARCS, DEGREES
        //IOGraph.getGraphStatsVertices(pg.graph,log);
        //IOGraph.getGraphStatsEdges(pg.graph,log) ;

        pg.computeBestTargetSensesOnGoldStandard(args[1]);
        log.debug("Direct Link + Double Max Flow + First Sense :");
        log.debug("\nTotal TP1 = " + pg.sumTP1 +
                "\nTotal FP1 = " + pg.sumFP1 +
                "\nTotal FN1 = " + pg.sumFN1 +
                "\nTotal TN1 = " + pg.sumTN1 +
                "\nTotal TP2 = " + pg.sumTP2 +
                "\nTotal FP2 = " + pg.sumFP2 +
                "\nTotal FN2 = " + pg.sumFN2 +
                "\nTotal TN2 = " + pg.sumTN2 +
                "\nTotal TP3 = " + pg.sumTP3 +
                "\nTotal FP3 = " + pg.sumFP3 +
                "\nTotal FN3 = " + pg.sumFN3 +
                "\nTotal TN3 = " + pg.sumTN3);
        log.debug("Precision 1 : " + pg.getPrecision1());
        log.debug("Recall 1 : " + pg.getRecall1());
        log.debug("F-Measure 1 : " + pg.getFMeasure1());
        log.debug("Accuracy 1 : " + pg.getAccuracy1());
        log.debug("Precision 2 : " + pg.getPrecision2());
        log.debug("Recall 2 : " + pg.getRecall2());
        log.debug("F-Measure 2 : " + pg.getFMeasure2());
        log.debug("Accuracy 2 : " + pg.getAccuracy2());
        log.debug("Precision 3 : " + pg.getPrecision3());
        log.debug("Recall 3 : " + pg.getRecall3());
        log.debug("F-Measure 3 : " + pg.getFMeasure3());
        log.debug("Accuracy 3 : " + pg.getAccuracy3());

    }

    public double getPrecision1(){
        return ((double)sumTP1)/((double)(sumTP1+sumFP1)) ;
    }

    public double getRecall1(){
        return ((double)sumTP1)/((double)(sumTP1+sumFN1)) ;
    }

    public double getFMeasure1(){
        double p = ((double)sumTP1)/((double)(sumTP1+sumFP1)) ;
        double r = ((double)sumTP1)/((double)(sumTP1+sumFN1)) ;
        return ((2*p*r)/(p+r)) ;
    }

    public double getAccuracy1(){
        return ((double)(sumTP1+sumTN1))/((double)(sumTP1+sumTN1+sumFP1+sumFN1)) ;
    }

    public double getPrecision2(){
        return ((double)sumTP2)/((double)(sumTP2+sumFP2)) ;
    }

    public double getRecall2(){
        return ((double)sumTP2)/((double)(sumTP2+sumFN2)) ;
    }

    public double getFMeasure2(){
        double p = ((double)sumTP2)/((double)(sumTP2+sumFP2)) ;
        double r = ((double)sumTP2)/((double)(sumTP2+sumFN2)) ;
        return ((2*p*r)/(p+r)) ;
    }

    public double getAccuracy2(){
        return ((double)(sumTP2+sumTN2))/((double)(sumTP2+sumTN2+sumFP2+sumFN2)) ;
    }

    public double getPrecision3(){
        return ((double)sumTP3)/((double)(sumTP3+sumFP3)) ;
    }

    public double getRecall3(){
        return ((double)sumTP3)/((double)(sumTP3+sumFN3)) ;
    }

    public double getFMeasure3(){
        double p = ((double)sumTP3)/((double)(sumTP3+sumFP3)) ;
        double r = ((double)sumTP3)/((double)(sumTP3+sumFN3)) ;
        return ((2*p*r)/(p+r)) ;
    }

    public double getAccuracy3(){
        return ((double)(sumTP3+sumTN3))/((double)(sumTP3+sumTN3+sumFP3+sumFN3)) ;
    }

    public void computeBestTargetSensesOnGoldStandard(String fileName) throws FileNotFoundException,IOException {
        BufferedReader buf = new BufferedReader(new FileReader(fileName)) ;
        String s = buf.readLine() ;
        Map<String,Map<String,List<String>>> map = new HashMap<>() ;
        while(s!=null){
            String[] sourceTarget = s.split(",") ;
            String sourceSense = sourceTarget[0] ;
            String targetEntry = sourceTarget[1] ;
            String[] expecSenses = sourceTarget[2].split(" ") ;
            List<String> expectedSenses = new ArrayList<>() ;
            for(int i = 0 ; i<expecSenses.length ; i++){
                expectedSenses.add(expecSenses[i]) ;
            }

            Map<String,List<String>> m = map.get(sourceSense) ;
            if(m!=null){ // sourceSense is already part of the sources added
                m.put(targetEntry,expectedSenses) ;
                map.put(sourceSense,m);
            }else{
                Map<String,List<String>> m2 = new HashMap<>() ;
                m2.put(targetEntry,expectedSenses);
                map.put(sourceSense,m2) ;
            }

            s = buf.readLine() ;
        }
        this.computeBestTargetSensesOnList(map);
    }

    public void computeRandomTargetSensesOnGoldStandard(String fileName) throws FileNotFoundException,IOException {
        BufferedReader buf = new BufferedReader(new FileReader(fileName)) ;
        String s = buf.readLine() ;
        Map<String,Map<String,List<String>>> map = new HashMap<>() ;
        while(s!=null){
            String[] sourceTarget = s.split(",") ;
            String sourceSense = sourceTarget[0] ;
            String targetEntry = sourceTarget[1] ;
            String[] expecSenses = sourceTarget[2].split(" ") ;
            List<String> expectedSenses = new ArrayList<>() ;
            for(int i = 0 ; i<expecSenses.length ; i++){
                expectedSenses.add(expecSenses[i]) ;
            }

            Map<String,List<String>> m = map.get(sourceSense) ;
            if(m!=null){ // sourceSense is already part of the sources added
                m.put(targetEntry,expectedSenses) ;
                map.put(sourceSense,m);
            }else{
                Map<String,List<String>> m2 = new HashMap<>() ;
                m2.put(targetEntry,expectedSenses);
                map.put(sourceSense,m2) ;
            }

            s = buf.readLine() ;
        }
        this.computeRandomTargetSensesOnList(map);
    }

    public void computeFirstTargetSensesOnGoldStandard(String fileName) throws FileNotFoundException,IOException {
        BufferedReader buf = new BufferedReader(new FileReader(fileName)) ;
        String s = buf.readLine() ;
        Map<String,Map<String,List<String>>> map = new HashMap<>() ;
        while(s!=null){
            String[] sourceTarget = s.split(",") ;
            String sourceSense = sourceTarget[0] ;
            String targetEntry = sourceTarget[1] ;
            String[] expecSenses = sourceTarget[2].split(" ") ;
            List<String> expectedSenses = new ArrayList<>() ;
            for(int i = 0 ; i<expecSenses.length ; i++){
                expectedSenses.add(expecSenses[i]) ;
            }

            Map<String,List<String>> m = map.get(sourceSense) ;
            if(m!=null){ // sourceSense is already part of the sources added
                m.put(targetEntry,expectedSenses) ;
                map.put(sourceSense,m);
            }else{
                Map<String,List<String>> m2 = new HashMap<>() ;
                m2.put(targetEntry,expectedSenses);
                map.put(sourceSense,m2) ;
            }

            s = buf.readLine() ;
        }
        this.computeFirstTargetSensesOnList(map);
    }

    public void computeBestTargetSensesOnGoldStandardSimple(String fileName) throws FileNotFoundException,IOException {
        BufferedReader buf = new BufferedReader(new FileReader(fileName)) ;
        String s = buf.readLine() ;
        Map<String,Map<String,List<String>>> map = new HashMap<>() ;
        while(s!=null){
            String[] sourceTarget = s.split(",") ;
            String sourceSense = sourceTarget[0] ;
            String targetEntry = sourceTarget[1] ;
            String[] expecSenses = sourceTarget[2].split(" ") ;
            List<String> expectedSenses = new ArrayList<>() ;
            for(int i = 0 ; i<expecSenses.length ; i++){
                expectedSenses.add(expecSenses[i]) ;
            }

            Map<String,List<String>> m = map.get(sourceSense) ;
            if(m!=null){ // sourceSense is already part of the sources added
                m.put(targetEntry,expectedSenses) ;
                map.put(sourceSense,m);
            }else{
                Map<String,List<String>> m2 = new HashMap<>() ;
                m2.put(targetEntry,expectedSenses);
                map.put(sourceSense,m2) ;
            }

            s = buf.readLine() ;
        }
        this.computeBestTargetSensesOnListSimple(map);
    }

    public void computeBestTargetSensesOnList(Map<String,Map<String,List<String>>> list){
        for(String source : list.keySet()){
            for(String target : list.get(source).keySet()){
                List<String> expectedSenses = list.get(source).get(target) ;
                //SimpleDirectedGraph<String,DefaultEdge> g = this.getComponent(source) ;
                this.computeBestTargetSenses(source,target,expectedSenses,simplfiedGraph, graph);
            }
        }
    }

    public void computeRandomTargetSensesOnList(Map<String,Map<String,List<String>>> list){
        for(String source : list.keySet()){
            for(String target : list.get(source).keySet()){
                List<String> expectedSenses = list.get(source).get(target) ;
                this.computeRandomTargetSenses(source,target,expectedSenses,graph);
            }
        }
    }

    public void computeFirstTargetSensesOnList(Map<String,Map<String,List<String>>> list){
        for(String source : list.keySet()){
            for(String target : list.get(source).keySet()){
                List<String> expectedSenses = list.get(source).get(target) ;
                this.computeFirstTargetSenses(source,target,expectedSenses,graph);
            }
        }
    }

    public void computeBestTargetSensesOnListSimple(Map<String,Map<String,List<String>>> list){
        for(String source : list.keySet()){
            for(String target : list.get(source).keySet()){
                List<String> expectedSenses = list.get(source).get(target) ;
                this.computeBestTargetSensesSimple(source,target,expectedSenses,graph);
            }
        }
    }

    public void computeFirstTargetSenses(String wsSource, String leTarget,List<String> expectedSenses, SimpleDirectedGraph<String,DefaultEdge> g){
        if(isSense(wsSource)) {
            Set<String> potentialTargets = new HashSet<>();
            for (DefaultEdge e : g.outgoingEdgesOf(leTarget)) {
                String targetSense = g.getEdgeTarget(e);
                if (isSense(targetSense)) {
                    potentialTargets.add(targetSense);
                }
            }
            Map<String, Double> map = this.getDirectReturnLinks(wsSource,potentialTargets,g) ;
            if(!hasMatches(map)){
                map = this.getFirstSense(leTarget,g);
                log.debug("FIND SENSES : No direct return link, we choose first sense") ;
            }

            EvaluateTPTNFNFP eval = new EvaluateTPTNFNFP(expectedSenses,map) ;
            sumTP1 = sumTP1+eval.getTP1() ;
            sumFP1 = sumFP1+eval.getFP1() ;
            sumFN1 = sumFN1+eval.getFN1() ;
            sumTN1 = sumTN1+eval.getTN1() ;
            sumTP2 = sumTP2+eval.getTP2() ;
            sumFP2 = sumFP2+eval.getFP2() ;
            sumFN2 = sumFN2+eval.getFN2() ;
            sumTN2 = sumTN2+eval.getTN2() ;
            sumTP3 = sumTP3+eval.getTP3() ;
            sumFP3 = sumFP3+eval.getFP3() ;
            sumFN3 = sumFN3+eval.getFN3() ;
            sumTN3 = sumTN3+eval.getTN3() ;
            String chooseSense = "";
            if (map != null) {
                for (String s : map.keySet()) {
                    chooseSense = chooseSense + s + " : " + map.get(s) + "\n";
                }
            }
            log.debug("Senses of " + leTarget + " translation of " + wsSource + " :\n" + chooseSense);
            log.debug("Evaluation :" +
                    "\nTP1 = "+eval.getTP1()+" FP1 = " + eval.getFP1()+" FN1 = " + eval.getFN1()+" TN1 = " + eval.getTN1()+
                    "\nTP2 = "+eval.getTP2()+" FP2 = " + eval.getFP2()+" FN2 = " + eval.getFN2()+" TN2 = " + eval.getTN2()+
                    "\nTP3 = "+eval.getTP3()+" FP3 = " + eval.getFP3()+" FN3 = " + eval.getFN3()+" TN3 = " + eval.getTN3()) ;
        }else {
            log.debug(wsSource+" is not a sense");
        }
    }

    private Map<String,Double> getFirstSense(String leTarget, SimpleDirectedGraph<String,DefaultEdge> g){
        List<String> allSenses = new ArrayList() ;
        int first = Integer.MAX_VALUE ;
        String firstSense = "" ;
        for(DefaultEdge e : g.outgoingEdgesOf(leTarget)){
            String sense = g.getEdgeTarget(e) ;
            if(isSense(sense)){
                allSenses.add(sense) ;
                int num = getSenseNumber(sense) ;
                if(num<first){
                    first=num ;
                    firstSense=sense ;
                }
            }
        }
        Map<String,Double> res = new HashMap<>() ;
        for(String s : allSenses){
            res.put(s,0.0) ;
        }
        res.put(firstSense,1.0) ;
        return res ;
    }

    private static int getSenseNumber(String sense){
        String[] split = sense.split("_") ;
        String n1 = split[3] ;
        split = n1.split("[a-z]*") ;
        String n2 = split[0] ;
        if(n1.equals(n2)){
            return Integer.parseInt(n1) ;
        }else{
            return Integer.parseInt(n2)+1 ;
        }

    }

    public void computeRandomTargetSenses(String wsSource, String leTarget,List<String> expectedSenses, SimpleDirectedGraph<String,DefaultEdge> g){
        if(isSense(wsSource)) {
            Map<String, Double> map = this.getRandomSense(leTarget,g);
            EvaluateTPTNFNFP eval = new EvaluateTPTNFNFP(expectedSenses,map) ;
            sumTP1 = sumTP1+eval.getTP1() ;
            sumFP1 = sumFP1+eval.getFP1() ;
            sumFN1 = sumFN1+eval.getFN1() ;
            sumTN1 = sumTN1+eval.getTN1() ;
            sumTP2 = sumTP2+eval.getTP2() ;
            sumFP2 = sumFP2+eval.getFP2() ;
            sumFN2 = sumFN2+eval.getFN2() ;
            sumTN2 = sumTN2+eval.getTN2() ;
            sumTP3 = sumTP3+eval.getTP3() ;
            sumFP3 = sumFP3+eval.getFP3() ;
            sumFN3 = sumFN3+eval.getFN3() ;
            sumTN3 = sumTN3+eval.getTN3() ;
            String chooseSense = "";
            if (map != null) {
                for (String s : map.keySet()) {
                    chooseSense = chooseSense + s + " : " + map.get(s) + "\n";
                }
            }
            log.debug("Senses of " + leTarget + " translation of " + wsSource + " :\n" + chooseSense);
            log.debug("Evaluation :" +
                    "\nTP1 = "+eval.getTP1()+" FP1 = " + eval.getFP1()+" FN1 = " + eval.getFN1()+" TN1 = " + eval.getTN1()+
                    "\nTP2 = "+eval.getTP2()+" FP2 = " + eval.getFP2()+" FN2 = " + eval.getFN2()+" TN2 = " + eval.getTN2()+
                    "\nTP3 = "+eval.getTP3()+" FP3 = " + eval.getFP3()+" FN3 = " + eval.getFN3()+" TN3 = " + eval.getTN3()) ;
        }else {
            log.debug(wsSource+" is not a sense");
        }
    }

    private Map<String,Double> getRandomSense(String leTarget, SimpleDirectedGraph<String,DefaultEdge> g){
        List<String> allSenses = new ArrayList() ;
        for(DefaultEdge e : g.outgoingEdgesOf(leTarget)){
            String sense = g.getEdgeTarget(e) ;
            if(isSense(sense)){
                allSenses.add(sense) ;
            }
        }
        int n = allSenses.size() ;
        int r = (int)(Math.random()*n) ;
        Map<String,Double> res = new HashMap<>() ;
        for(String s : allSenses){
            res.put(s,0.0) ;
        }
        res.put(allSenses.get(r),1.0) ;
        return res ;
    }

    public void computeBestTargetSenses(String wsSource, String leTarget,List<String> expectedSenses, SimpleDirectedGraph<String,DefaultEdge> simplifiedG, SimpleDirectedGraph<String,DefaultEdge> g){
        if(isSense(wsSource)) {
            Map<String, Double> map = this.doubleMaxFlowPaths(wsSource, leTarget,simplifiedG, g);
            EvaluateTPTNFNFP eval = new EvaluateTPTNFNFP(expectedSenses,map) ;
            sumTP1 = sumTP1+eval.getTP1() ;
            sumFP1 = sumFP1+eval.getFP1() ;
            sumFN1 = sumFN1+eval.getFN1() ;
            sumTN1 = sumTN1+eval.getTN1() ;
            sumTP2 = sumTP2+eval.getTP2() ;
            sumFP2 = sumFP2+eval.getFP2() ;
            sumFN2 = sumFN2+eval.getFN2() ;
            sumTN2 = sumTN2+eval.getTN2() ;
            sumTP3 = sumTP3+eval.getTP3() ;
            sumFP3 = sumFP3+eval.getFP3() ;
            sumFN3 = sumFN3+eval.getFN3() ;
            sumTN3 = sumTN3+eval.getTN3() ;
            String chooseSense = "";
            if (map != null) {
                for (String s : map.keySet()) {
                    chooseSense = chooseSense + s + " : " + map.get(s) + "\n";
                }
            }
            log.debug("Senses of " + leTarget + " translation of " + wsSource + " :\n" + chooseSense);
            log.debug("Evaluation :" +
                    "\nTP1 = "+eval.getTP1()+" FP1 = " + eval.getFP1()+" FN1 = " + eval.getFN1()+" TN1 = " + eval.getTN1()+
                    "\nTP2 = "+eval.getTP2()+" FP2 = " + eval.getFP2()+" FN2 = " + eval.getFN2()+" TN2 = " + eval.getTN2()+
                    "\nTP3 = "+eval.getTP3()+" FP3 = " + eval.getFP3()+" FN3 = " + eval.getFN3()+" TN3 = " + eval.getTN3()) ;
        }else {
            log.debug(wsSource+" is not a sense");
        }
    }

    public void computeBestTargetSensesSimple(String wsSource, String leTarget,List<String> expectedSenses, SimpleDirectedGraph<String,DefaultEdge> g){
        if(isSense(wsSource)) {
            Map<String, Double> map = this.simpleMaxFlowPaths(wsSource, leTarget,g);
            EvaluateTPTNFNFP eval = new EvaluateTPTNFNFP(expectedSenses,map) ;
            sumTP1 = sumTP1+eval.getTP1() ;
            sumFP1 = sumFP1+eval.getFP1() ;
            sumFN1 = sumFN1+eval.getFN1() ;
            sumTN1 = sumTN1+eval.getTN1() ;
            sumTP2 = sumTP2+eval.getTP2() ;
            sumFP2 = sumFP2+eval.getFP2() ;
            sumFN2 = sumFN2+eval.getFN2() ;
            sumTN2 = sumTN2+eval.getTN2() ;
            sumTP3 = sumTP3+eval.getTP3() ;
            sumFP3 = sumFP3+eval.getFP3() ;
            sumFN3 = sumFN3+eval.getFN3() ;
            sumTN3 = sumTN3+eval.getTN3() ;
            String chooseSense = "";
            if (map != null) {
                for (String s : map.keySet()) {
                    chooseSense = chooseSense + s + " : " + map.get(s) + "\n";
                }
            }
            log.debug("Senses of " + leTarget + " translation of " + wsSource + " :\n" + chooseSense);
            log.debug("Evaluation :" +
                    "\nTP1 = "+eval.getTP1()+" FP1 = " + eval.getFP1()+" FN1 = " + eval.getFN1()+" TN1 = " + eval.getTN1()+
                    "\nTP2 = "+eval.getTP2()+" FP2 = " + eval.getFP2()+" FN2 = " + eval.getFN2()+" TN2 = " + eval.getTN2()+
                    "\nTP3 = "+eval.getTP3()+" FP3 = " + eval.getFP3()+" FN3 = " + eval.getFN3()+" TN3 = " + eval.getTN3()) ;
        }else {
            log.debug(wsSource+" is not a sense");
        }
    }

    public SimpleDirectedGraph<String,DefaultEdge> getSubGraph(Set<String> vertices){ // return the subgraph de this.graph engendre par vertices
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        for(String v : vertices){
            res.addVertex(v) ;
        }
        for(String v1 : res.vertexSet()){
            for(String v2 : res.vertexSet()){
                if(this.graph.containsEdge(v1,v2)){
                    res.addEdge(v1,v2) ;
                }
            }
        }
        return res ;
    }

    public static SimpleDirectedGraph<String,DefaultEdge> getSubGraph(Set<String> vertices,SimpleDirectedGraph<String,DefaultEdge> graph){ // return the subgraph de this.graph engendre par vertices
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        for(String v : vertices){
            res.addVertex(v) ;
        }
        for(String v1 : res.vertexSet()){
            for(String v2 : res.vertexSet()){
                if(graph.containsEdge(v1,v2)){
                    res.addEdge(v1,v2) ;
                }
            }
        }
        return res ;
    }

    public Set<String> getNodesOnPath(Map<DefaultEdge,Double> map,SimpleDirectedGraph<String,DefaultEdge> graph){
        Set<String> nodes = new HashSet<>() ;
        for(DefaultEdge e : map.keySet()){
            if(map.get(e)>0){
                String source = graph.getEdgeSource(e) ;
                String target = graph.getEdgeTarget(e) ;
                nodes.add(source) ;
                nodes.add(target) ;
            }
        }
        return nodes ;
    }

    public double nbEdgeDistinctPaths(String source, String sink){
        if(isSense(source) && !isSense(sink)){
            String sourceLexEntry = this.getLexicalEntry(source) ;
            SimpleDirectedGraph<String,DefaultEdge> g = this.removeVertex(sourceLexEntry) ;
            MaximumFlowAlgorithm<String,DefaultEdge> mf = new EdmondsKarpMFImpl(g) ;
            return mf.calculateMaximumFlow(source,sink) ;
        }else{
            return -1 ;
        }
    }

    public double nbEdgeDistinctPaths(String source, String sink, SimpleDirectedGraph<String,DefaultEdge> g){
        if(isSense(source) && !isSense(sink)){
            if(g.vertexSet().isEmpty()){
                return 0 ;
            }else {
                SimpleDirectedGraph<String,DefaultEdge> g2 = removeVertex(this.getLexicalEntry(source),g) ;
                MaximumFlowAlgorithm<String, DefaultEdge> mf = new PushRelabelMFImpl<String, DefaultEdge>(g2);
                return mf.calculateMaximumFlow(source,sink) ;
            }
        }else{
            return -1 ;
        }
    }

    public Map<DefaultEdge,Double> flowOnEdges(String source, String sink){
        if(isSense(source) && !isSense(sink)){
            SimpleDirectedGraph<String,DefaultEdge> g = this.removeVertex(this.getLexicalEntry(source)) ;
            MaximumFlowAlgorithm<String,DefaultEdge> mf = new EdmondsKarpMFImpl(g) ;
            return mf.getMaximumFlow(source,sink).getFlow() ;
        }else{
            return null ;
        }
    }

    public Map<DefaultEdge,Double> flowOnEdges2(String source, String sink){
        if(isSense(source) && !isSense(sink)){
            SimpleDirectedGraph<String,DefaultEdge> g = this.removeVertices(source,sink) ;
            MaximumFlowAlgorithm<String,DefaultEdge> mf = new EdmondsKarpMFImpl(g) ;
            return mf.getMaximumFlow(source,sink).getFlow() ;
        }else{
            return null ;
        }
    }

    public Map<DefaultEdge,Double> flowOnEdges(String source, String sink, SimpleDirectedGraph<String,DefaultEdge> graph){
        if(isSense(source) && !isSense(sink)){
            if(graph.vertexSet().isEmpty()){
                return new HashMap<>() ;
            }else {
                SimpleDirectedGraph<String,DefaultEdge> g = removeVertex(this.getLexicalEntry(source),graph) ;
                MaximumFlowAlgorithm<String, DefaultEdge> mf = new PushRelabelMFImpl<String, DefaultEdge>(g);
                return mf.getMaximumFlow(source, sink).getFlow();
            }
        }else{
            return null ;
        }
    }

    private String getLexicalEntry(String sense){
        if(isSense(sense)){
            Set<DefaultEdge> incoming = this.graph.incomingEdgesOf(sense) ;
            Iterator<DefaultEdge> inEdges = incoming.iterator() ;
            while(inEdges.hasNext()){
                DefaultEdge e = inEdges.next() ;
                return this.graph.getEdgeSource(e) ;
            }
        }
        return null ;
    }

    private SimpleDirectedGraph<String,DefaultEdge> removeVertex(String tobeRemoved){
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        for(DefaultEdge e : this.graph.edgeSet()){
            String source = this.graph.getEdgeSource(e) ;
            String target = this.graph.getEdgeTarget(e) ;
            res.addVertex(source) ;
            res.addVertex(target) ;
            if(!source.equals(tobeRemoved) && !target.equals(tobeRemoved)) {
                res.addEdge(source, target);
            }
        }
        return res ;
    }

    private static SimpleDirectedGraph<String,DefaultEdge> removeVertex(String tobeRemoved, SimpleDirectedGraph<String,DefaultEdge> g){
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        for(String v : g.vertexSet()){
            res.addVertex(v) ;
        }
        for(DefaultEdge e : g.edgeSet()){
            String source = g.getEdgeSource(e) ;
            String target = g.getEdgeTarget(e) ;
            //res.addVertex(source) ;
            //res.addVertex(target) ;
            if(!source.equals(tobeRemoved) && !target.equals(tobeRemoved)) {
                res.addEdge(source, target);
            }
        }
        return res ;
    }

    private SimpleDirectedGraph<String,DefaultEdge> removeVertices(String sourceSense, String targetEntry){
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        res.addVertex(sourceSense);
        res.addVertex(targetEntry) ;
        for(DefaultEdge e : this.graph.edgeSet()){
            String source = this.graph.getEdgeSource(e) ;
            String target = this.graph.getEdgeTarget(e) ;
            if(!getLanguage(source).equals(getLanguage(sourceSense)) && !getLanguage(source).equals(getLanguage(targetEntry))){
                res.addVertex(source);
                if(!getLanguage(target).equals(getLanguage(sourceSense)) && !getLanguage(target).equals(getLanguage(targetEntry))) {
                    res.addVertex(target) ;
                    res.addEdge(source, target);
                }
            }else{
                if(!getLanguage(target).equals(getLanguage(sourceSense)) && !getLanguage(target).equals(getLanguage(targetEntry))) {
                    res.addVertex(target) ;
                }
            }
        }
        return res ;
    }

    public SimpleDirectedGraph<String,DefaultEdge> getExportableGraph(){
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        Map<String,String> mapNames = new HashMap<>() ;
        int nextle = 0 ;
        int nextws = 0 ;
        for(DefaultEdge e : this.graph.edgeSet()){
            String source = this.graph.getEdgeSource(e) ;
            String target = this.graph.getEdgeTarget(e) ;

            String newSource = mapNames.get(source) ;
            if(newSource==null) {
                if (isSense(source)) {
                    newSource = "S" + nextws;
                    nextws = nextws + 1;
                    mapNames.put(source, newSource);
                }else{
                    newSource = "L" + nextle;
                    nextle = nextle + 1;
                    mapNames.put(source, newSource);
                }
            }
            res.addVertex(newSource) ;

            String newTarget = mapNames.get(target) ;
            if(newTarget==null){
                if (isSense(target)) {
                    newTarget = "S" + nextws;
                    nextws = nextws + 1;
                    mapNames.put(target, newTarget);
                }else{
                    newTarget = "L" + nextle;
                    nextle = nextle + 1;
                    mapNames.put(target, newTarget);
                }
            }
            res.addVertex(newTarget) ;

            res.addEdge(newSource,newTarget) ;
        }
        return res ;
    }


    private static boolean isSense(String v){
        return v.charAt(35)=='_' ;
    }

    private static String getLanguage(String v){
        return v.split("/")[4] ;
    }

    public static Model getsubGraph(Model m, List<Resource> sourceList, int depth){
        Model res = ModelFactory.createDefaultModel() ;
        Set<Resource> vertices = new HashSet<>() ;
        for(Resource source : sourceList) {
            vertices.addAll(getSubGraphVertices(m, source, depth));
        }
        Set<Resource> tmp = new HashSet<>() ;
        for(Resource r : vertices){
            StmtIterator stmt = m.listStatements(r,LemonOnt.sense, (RDFNode) null);
            while(stmt.hasNext()){
                Statement next = stmt.next() ;
                Resource sense = next.getResource() ;
                tmp.add(sense) ;
            }
        }
        vertices.addAll(tmp) ;
        for(Resource r1 : vertices){
            for(Resource r2 : vertices){
                StmtIterator stmtIter = m.listStatements(r1, VarTransOnt.translatableAs,r2) ;
                while(stmtIter.hasNext()){
                    Statement stm = stmtIter.next() ;
                    res.add(stm) ;
                }
                stmtIter = m.listStatements(r1,LemonOnt.sense,r2) ;
                while(stmtIter.hasNext()){
                    Statement stm = stmtIter.next() ;
                    res.add(stm) ;
                }
            }
        }
        return res ;
    }

    private static Set<Resource> getSubGraphVertices(Model m, Resource source, int depth){
        Set<Resource> res = new HashSet<>() ;
        res.add(source) ;
        if(depth==0){
            return res ;
        }else{
            StmtIterator stmtIter = m.listStatements(source, VarTransOnt.translatableAs, (RDFNode) null) ;
            while(stmtIter.hasNext()){
                Statement stm = stmtIter.next() ;
                Resource newSource = stm.getResource() ; // get the new source
                res.addAll(getSubGraphVertices(m,newSource,depth-1)) ;
            }
            stmtIter = m.listStatements(source, LemonOnt.sense, (RDFNode) null) ;
            while(stmtIter.hasNext()){
                Statement stm = stmtIter.next() ;
                Resource newSource = stm.getResource() ; // get the new source
                res.addAll(getSubGraphVertices(m,newSource,depth-1)) ;
            }

            return res ;
        }
    }

    public static SimpleDirectedGraph<String,DefaultEdge> getJGraph(Model m){
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        StmtIterator stmtIter = m.listStatements() ;
        while(stmtIter.hasNext()){
            Statement stm = stmtIter.next() ;
            Resource subject =  stm.getSubject() ;
            String source = subject.toString() ;
            Resource resource = stm.getResource() ;
            String target = resource.toString() ;
            res.addVertex(source) ;
            res.addVertex(target) ;
            if(!source.equals(target)) {
                res.addEdge(source, target);
            }
        }
        return res ;
    }

    public static SimpleGraph<String,DefaultEdge> getUJGraph(Model m){
        SimpleGraph<String,DefaultEdge> res = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class) ;
        StmtIterator stmtIter = m.listStatements() ;
        while(stmtIter.hasNext()){
            Statement stm = stmtIter.next() ;
            Resource subject =  stm.getSubject() ;
            String source = subject.toString() ;
            Resource resource = stm.getResource() ;
            String target = resource.toString() ;
            res.addVertex(source) ;
            res.addVertex(target) ;
            res.addEdge(source, target) ;
        }
        return res ;
    }

    private double computeNumberPaths(String sense1,String sense2){
        if(isSense(sense1) && isSense(sense2)){
            double s1s2 = this.nbEdgeDistinctPaths(sense1,this.getLexicalEntry(sense2)) ;
            double s2s1 = this.nbEdgeDistinctPaths(sense2,this.getLexicalEntry(sense1)) ;
            if(s1s2>=0 && s2s1>=0){
                return s1s2+s2s1 ;
            }else{
                return -1 ;
            }
        }else{
            return -1 ;
        }
    }

    private double computeNumberPaths(String sense1,String sense2,SimpleDirectedGraph<String,DefaultEdge> graph){
        if(isSense(sense1) && isSense(sense2)){
            double s1s2 = 0 ;
            double s2s1 = 0 ;
            s1s2 = s1s2 + nbEdgeDistinctPaths(sense1, this.getLexicalEntry(sense2), graph);
            s2s1 = s2s1 + nbEdgeDistinctPaths(sense2, this.getLexicalEntry(sense1), graph);
            if(s1s2>=0 && s2s1>=0){
                return s1s2+s2s1 ;
            }else{
                return -1 ;
            }

        }else{
            return -1 ;
        }
    }

    private double computeNumberPathsSample(String sense1,String sense2,SimpleDirectedGraph<String,DefaultEdge> graph){
        if(isSense(sense1) && isSense(sense2)){
            double s1s2 = 0 ;
            double s2s1 = 0 ;
            for(int i = 0 ; i<this.s ; i++) {
                SimpleDirectedGraph<String,DefaultEdge> g = sampleGraph(graph,this.p) ;
                s1s2 = s1s2 + nbEdgeDistinctPaths(sense1, this.getLexicalEntry(sense2), g);
                s2s1 = s2s1 + nbEdgeDistinctPaths(sense2, this.getLexicalEntry(sense1), g);
            }
            if(s1s2>=0 && s2s1>=0){
                return s1s2+s2s1 ;
            }else{
                return -1 ;
            }

        }else{
            return -1 ;
        }
    }

    private static SimpleDirectedGraph<String,DefaultEdge> sampleGraph(SimpleDirectedGraph<String,DefaultEdge> g, double p){
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        for(String v : g.vertexSet()){
            res.addVertex(v) ;
        }
        for(DefaultEdge e : g.edgeSet()){
            String source = g.getEdgeSource(e) ;
            String target = g.getEdgeTarget(e) ;
            double d = Math.random();
            if(d<p){
                res.addEdge(source,target) ;
            }
        }
        return res ;
    }

    private Set<String> computeVerticesOnPaths(String sense1,String sense2,SimpleDirectedGraph<String,DefaultEdge> g){
        if(isSense(sense1) && isSense(sense2)){
            Set<String> paths1 = this.getNodesOnPath(this.flowOnEdges(sense1,this.getLexicalEntry(sense2),g),g) ;
            Set<String> paths2 = this.getNodesOnPath(this.flowOnEdges(sense2,this.getLexicalEntry(sense1),g),g) ;
            paths1.retainAll(paths2) ;
            if(!paths1.isEmpty()){
                paths1.add(sense1) ;
                paths1.add(sense2) ;
                paths1.add(this.getLexicalEntry(sense1)) ;
                paths1.add(this.getLexicalEntry(sense2)) ;
            }
            return paths1 ;
        }else{
            return null ;
        }
    }

    public Map<String,Double> simpleMaxFlowPaths(String sourceSense, String targetEntry, SimpleDirectedGraph<String,DefaultEdge> graph){
        if(isSense(sourceSense) && !isSense(targetEntry) && this.graph.containsEdge(sourceSense,targetEntry)) {
            Set<String> potentialTargets = new HashSet<>();
            for (DefaultEdge e : graph.outgoingEdgesOf(targetEntry)) {
                String targetSense = graph.getEdgeTarget(e);
                if (isSense(targetSense)) {
                    potentialTargets.add(targetSense);
                }
            }
            Map<String, Double> res = new HashMap<>();
            for (String targetSense : potentialTargets) {
                Double d = this.computeNumberPaths(sourceSense, targetSense,graph);
                res.put(targetSense, d);
            }
            return res;
        }else{
            log.debug("RETURN NULL : "+sourceSense+" is not a sense OR "+targetEntry+" is a sense OR there is no edge between them.") ;
            return null ;
        }
    }

    private Map<String,Double> getDirectReturnLinks(String sourceSense, Set<String> potentialTargets, SimpleDirectedGraph<String,DefaultEdge> graph){
        Map<String, Double> res = new HashMap<>() ;
        String sourceEntry = getLexicalEntry(sourceSense) ;
        for(String s : potentialTargets){
            if(graph.containsEdge(s,sourceEntry)){
                res.put(s,1.0) ;
            }else {
                res.put(s, 0.0);
            }
        }
        return res ;
    }

    public Map<String,Double> doubleMaxFlowPaths(String sourceSense, String targetEntry,SimpleDirectedGraph<String,DefaultEdge> simplifiedGraph , SimpleDirectedGraph<String,DefaultEdge> graph){ // looks for paths both ways, and look for paths again going only through vertices that were in both way paths
        if(isSense(sourceSense) && !isSense(targetEntry) && graph.containsEdge(sourceSense,targetEntry)) {
            Set<String> toAdd = new HashSet<>() ;
            Set<String> potentialTargets = new HashSet<>();
            /*log.debug("potentialTargets :") ;
            for (DefaultEdge e : graph.outgoingEdgesOf(targetEntry)) {
                String targetSense = graph.getEdgeTarget(e);
                if (isSense(targetSense)) {
                    potentialTargets.add(targetSense);
                    log.debug(targetSense) ;
                }
            }*/
            toAdd.addAll(potentialTargets) ;
            toAdd.add(sourceSense) ;
            toAdd.add(getLexicalEntry(sourceSense)) ;
            toAdd.add(targetEntry) ;
            SimpleDirectedGraph<String,DefaultEdge> g2 = ToolsGraph.addVertices(toAdd,simplifiedGraph,graph) ;
            Map<String, Double> res = getDirectReturnLinks(sourceSense,potentialTargets,graph) ;
            if(!hasMatches(res)) {
                if(g2.inDegreeOf(getLexicalEntry(sourceSense)) > 0 && g2.outDegreeOf(sourceSense) > 0 && g2.inDegreeOf(targetEntry) > 0) {
                    for (String targetSense : potentialTargets) {
                        if (g2.outDegreeOf(targetSense) > 0) {
                            Set<String> vertices = this.computeVerticesOnPaths(sourceSense, targetSense, g2);
                            SimpleDirectedGraph<String, DefaultEdge> g = getSubGraph(vertices, g2);
                            Double d = this.computeNumberPaths(sourceSense, targetSense, g);
                            res.put(targetSense, d);
                        }
                    }
                }
                if (!hasMatches(res)) { // if no sense matches, we get the first one
                    log.debug("FIND SENSES : First Senses") ;
                    return getFirstSense(targetEntry, graph);
                } else {
                    log.debug("FIND SENSES : Max Flow") ;
                    return res;
                }
            }else{
                log.debug("FIND SENSES : Direct Link") ;
                return res ;
            }
        }else{
            log.debug("RETURN NULL : "+sourceSense+" is not a sense OR "+targetEntry+" is a sense OR there is no edge between them.") ;
            return null ;
        }
    }

    public Map<String,Double> doubleMaxFlowPathsSample(String sourceSense, String targetEntry, SimpleDirectedGraph<String,DefaultEdge> graph){ // looks for paths both ways, and look for paths again going only through vertices that were in both way paths
        if(isSense(sourceSense) && !isSense(targetEntry) && graph.containsEdge(sourceSense,targetEntry)) {
            Set<String> potentialTargets = new HashSet<>();
            for (DefaultEdge e : graph.outgoingEdgesOf(targetEntry)) {
                String targetSense = graph.getEdgeTarget(e);
                if (isSense(targetSense)) {
                    potentialTargets.add(targetSense);
                }
            }
            Map<String, Double> res = new HashMap<>();
            for (String targetSense : potentialTargets) {
                Set<String> vertices = this.computeVerticesOnPaths(sourceSense,targetSense,graph) ;
                SimpleDirectedGraph<String,DefaultEdge> g = getSubGraph(vertices,graph) ;
                Double d = this.computeNumberPathsSample(sourceSense, targetSense,g);
                res.put(targetSense, d);
            }
            if(!hasMatches(res)){ // if no sense matches, we get the first one
                return getFirstSense(targetEntry,graph) ;
            }else {
                return res;
            }
        }else{
            log.debug("RETURN NULL : "+sourceSense+" is not a sense OR "+targetEntry+" is a sense OR there is no edge between them.") ;
            return null ;
        }
    }

    private static boolean hasMatches(Map<String,Double> map){
        boolean res = false ;
        for(String s : map.keySet()){
            double d = map.get(s) ;
            res = res||(d>=0.5) ;
        }
        return res ;
    }
}
