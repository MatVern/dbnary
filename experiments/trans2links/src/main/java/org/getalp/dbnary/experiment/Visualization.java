package org.getalp.dbnary.experiment;


import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDBFactory;
import org.jgrapht.Graph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.ext.*;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

public class Visualization {

    private static Logger log = LoggerFactory.getLogger(ToolsGraph.class);

    public static void main(String args[])throws FileNotFoundException,IOException {
        log.debug("Getting weighted graph from file...") ;
        SimpleWeightedGraph<String,DefaultWeightedEdge> wg = ToolsGraph.getWeightedGraph() ;
        log.debug(wg.vertexSet().size()+" vertices in the graph") ;
        log.debug(ToolsGraph.seeWeightedGraph(wg)) ;

        log.debug("Getting connected components...");
        ConnectivityInspector<String,DefaultWeightedEdge> ci = new ConnectivityInspector<String, DefaultWeightedEdge>(wg) ;
        List<Set<String>> components = ci.connectedSets() ;
        String connectComp = "" ;
        for(Set<String> comp : components){
            connectComp = connectComp+"{ " ;
            for(String node : comp){
                connectComp = connectComp+node+" " ;
            }
            connectComp = connectComp+"}\n" ;
        }
        log.debug(components.size()+" connected component(s)\n"+connectComp);

        wg = getExportableGraph(wg) ;
        log.debug(ToolsGraph.seeWeightedGraph(wg)) ;
        log.debug(wg.vertexSet().size()+" vertices in the renamed graph") ;


        log.debug("Getting clusters...") ;
        Set<Set<String>> minCut = ToolsGraph.minCutClustering(wg,3) ; //minCutClustering(wg,20) ;
        String clusters = "" ;
        for(Set<String> cluster : minCut){
            clusters = clusters+"{ " ;
            for(String v : cluster){
                clusters = clusters+v+" " ;
            }
            clusters = clusters+"}\n" ;
        }
        log.debug("Clusters : "+clusters);
    }

    public static SimpleWeightedGraph<String,DefaultWeightedEdge> getExportableGraph(SimpleWeightedGraph<String,DefaultWeightedEdge> g){
        SimpleWeightedGraph<String,DefaultWeightedEdge> res = new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class) ;
        Map<String,String> mapNames = new HashMap<>() ;
        int nextName = 0 ;
        for(DefaultWeightedEdge e : g.edgeSet()){
            String source = g.getEdgeSource(e) ;
            String target = g.getEdgeTarget(e) ;
            double d = g.getEdgeWeight(e) ;

            String newSource = mapNames.get(source) ;
            if(newSource==null){
                newSource = ""+nextName ;
                nextName = nextName+1 ;
                mapNames.put(source,newSource) ;
            }
            res.addVertex(newSource) ;

            String newTarget = mapNames.get(target) ;
            if(newTarget==null){
                newTarget = ""+nextName ;
                nextName = nextName+1 ;
                mapNames.put(target,newTarget) ;
            }
            res.addVertex(newTarget) ;

            DefaultWeightedEdge newEdge = res.addEdge(newSource,newTarget) ;
            res.setEdgeWeight(newEdge,d);
        }

        String out = "" ;
        for(String s : mapNames.keySet()){
            out = out+s+" -> "+mapNames.get(s)+"\n" ;
        }
        log.debug(out); ;
        return res ;
    }
}
