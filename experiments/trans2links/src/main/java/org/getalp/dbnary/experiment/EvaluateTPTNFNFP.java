package org.getalp.dbnary.experiment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EvaluateTPTNFNFP {
    private Map<String,Double> scores ; // For a given Source Sense, List of possible target senses sor one lexical entry with their score
    private List<String> expectedSenses ;
    private int tp1 ; // number of translations we caught (taking only the higest score)
    private int fp1 ; // number of translations we caught and were wrong (taking only the higest score)
    private int fn1 ; // number of translations we should have caught (taking only the higest score)
    private int tn1 ; // number of translations we didn't catch and were right (taking only the higest score)
    private int tp2 ; // number of translations we caught (taking the two higest scores)
    private int fp2 ; // number of translations we caught and were wrong (taking the two higest scores)
    private int fn2 ; // number of translations we should have caught (taking the two higest scores)
    private int tn2 ; // number of translations we didn't catch and were right (taking the two higest scores)
    private int tp3 ; // number of translations we caught (taking all positive scores)
    private int fp3 ; // number of translations we caught and were wrong (taking all positive scores)
    private int fn3 ; // number of translations we should have caught (taking all positive scores)
    private int tn3 ; // number of translations we didn't catch and were right (taking all positive scores)
    private Double highestScore ;
    private Double secondHighestScore ;

    public EvaluateTPTNFNFP(List<String> expected, Map<String,Double> score){
        tp1 = 0 ;
        fp1 = 0 ;
        fn1 = 0 ;
        tn1 = 0 ;
        tp2 = 0 ;
        fp2 = 0 ;
        fn2 = 0 ;
        tn2 = 0 ;
        tp3 = 0 ;
        fp3 = 0 ;
        fn3 = 0 ;
        tn3 = 0 ;
        expectedSenses = expected ;
        scores = score ;
        this.getHighestScores();
        this.evaluateScores();
    }

    private void getHighestScores(){
        Double highest = 0.0 ;
        Double second = 0.0 ;
        if(scores!=null) {
            for (String s : scores.keySet()) {
                Double d = scores.get(s);
                if (d > highest) {
                    second = highest;
                    highest = d;
                } else {
                    if (d < highest && d > second) {
                        second = d;
                    }
                }
            }
        }
        highestScore = highest ;
        secondHighestScore = second ;
    }

    private void evaluateScores(){ // give values to tp, fp, fn, tn
        if(scores!=null) {
            for (String s : scores.keySet()) {
                if (scores.get(s) > 0.0 && scores.get(s).equals(highestScore)) {
                    if (expectedSenses.contains(s)) {
                        tp1 = tp1 + 1;
                        tp2 = tp2 + 1;
                        tp3 = tp3 + 1;
                    } else {
                        fp1 = fp1 + 1;
                        fp2 = fp2 + 1;
                        fp3 = fp3 + 1;
                    }
                } else if (scores.get(s) > 0.0 && scores.get(s).equals(secondHighestScore)) {
                    if (expectedSenses.contains(s)) {
                        fn1 = fn1 + 1;
                        tp2 = tp2 + 1;
                        tp3 = tp3 + 1;
                    } else {
                        tn1 = tn1 + 1;
                        fp2 = fp2 + 1;
                        fp3 = fp3 + 1;
                    }
                } else if (scores.get(s) > 0.0) {
                    if (expectedSenses.contains(s)) {
                        fn1 = fn1 + 1;
                        fn2 = fn2 + 1;
                        tp3 = tp3 + 1;
                    } else {
                        tn1 = tn1 + 1;
                        tn2 = tn2 + 1;
                        fp3 = fp3 + 1;
                    }
                } else {
                    if (expectedSenses.contains(s)) {
                        fn1 = fn1 + 1;
                        fn2 = fn2 + 1;
                        fn3 = fn3 + 1;
                    } else {
                        tn1 = tn1 + 1;
                        tn2 = tn2 + 1;
                        tn3 = tn3 + 1;
                    }
                }
            }
        }
    }

    public int getTP1(){
        return tp1 ;
    }

    public int getFP1(){
        return fp1 ;
    }

    public int getFN1(){
        return fn1 ;
    }

    public int getTN1(){
        return tn1 ;
    }

    public int getTP2(){
        return tp2 ;
    }

    public int getFP2(){
        return fp2 ;
    }

    public int getFN2(){
        return fn2 ;
    }

    public int getTN2(){
        return tn2 ;
    }

    public int getTP3(){
        return tp3 ;
    }

    public int getFP3(){
        return fp3 ;
    }

    public int getFN3(){
        return fn3 ;
    }

    public int getTN3(){
        return tn3 ;
    }
}
