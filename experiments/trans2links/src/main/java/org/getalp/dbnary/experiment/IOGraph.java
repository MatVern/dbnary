package org.getalp.dbnary.experiment;


import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.tdb.TDBFactory;
import org.getalp.dbnary.tools.CounterSet;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.slf4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class IOGraph {

    public static void exportGraphToText(String fileTDB, String fileName) throws IOException {
        Dataset dataset = TDBFactory.createDataset(fileTDB) ;
        dataset.begin(ReadWrite.READ) ;
        Model m = dataset.getDefaultModel() ;
        File f = new File(fileName);
        f.createNewFile();
        FileWriter fw=new FileWriter(f);
        StmtIterator stmtIter = m.listStatements() ;
        while(stmtIter.hasNext()) {
            Statement stm = stmtIter.next();
            Resource subject = stm.getSubject();
            String source = subject.toString();
            Resource resource = stm.getResource();
            String target = resource.toString();
            fw.write(source + " " + target + "\n");
        }
        fw.close();
    }

    public static SimpleDirectedGraph<String,DefaultEdge> importGraphFromText(String fileName) throws FileNotFoundException, IOException {
        SimpleDirectedGraph<String,DefaultEdge> res = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class) ;
        BufferedReader buf = new BufferedReader(new FileReader(fileName)) ;
        String line = buf.readLine() ;
        while(line!=null){
            String[] split = line.split(" ") ;
            String source = split[0] ;
            String target = split[1] ;
            res.addVertex(source) ;
            res.addVertex(target) ;
            if(!source.equals(target)) {
                res.addEdge(source, target);
            }
            line = buf.readLine() ;
        }
        return res ;
    }

    private static String getLanguage(String v){
        return v.split("/")[4] ;
    }

    private static boolean isSense(String v){
        return v.charAt(35)=='_' ;
    }

    public static void getGraphStatsEdges(SimpleDirectedGraph<String,DefaultEdge> g, Logger log){
        Map<String,CounterSet> lele = new HashMap<>() ;
        Map<String,CounterSet> lews = new HashMap<>() ;
        Map<String,CounterSet> wsle = new HashMap<>() ;
        Map<String,CounterSet> wsws = new HashMap<>() ; // should be empty
        for(DefaultEdge e : g.edgeSet()){
            String source = g.getEdgeSource(e) ;
            String target = g.getEdgeTarget(e) ;
            String sourceLang = getLanguage(source) ;
            String targetLang = getLanguage(target) ;
            if(isSense(source)){
                if(isSense(target)){ // WSWS should never happen
                    log.debug("------------- WSWS : "+source+" -> "+target) ;
                    CounterSet cs = wsws.get(sourceLang) ;
                    if(cs != null){
                        cs.incr(targetLang) ;
                        wsws.put(sourceLang,cs) ;
                    }else{
                        cs = new CounterSet() ;
                        cs.incr(targetLang) ;
                        wsws.put(sourceLang,cs) ;
                    }
                }else{ // WSLE
                    CounterSet cs = wsle.get(sourceLang) ;
                    if(cs != null){
                        cs.incr(targetLang) ;
                        wsle.put(sourceLang,cs) ;
                    }else{
                        cs = new CounterSet() ;
                        cs.incr(targetLang) ;
                        wsle.put(sourceLang,cs) ;
                    }
                }
            }else{
                if(isSense(target)){ // LEWS
                    CounterSet cs = lews.get(sourceLang) ;
                    if(cs != null){
                        cs.incr(targetLang) ;
                        lews.put(sourceLang,cs) ;
                    }else{
                        cs = new CounterSet() ;
                        cs.incr(targetLang) ;
                        lews.put(sourceLang,cs) ;
                    }
                }else{ // LELE
                    CounterSet cs = lele.get(sourceLang) ;
                    if(cs != null){
                        cs.incr(targetLang) ;
                        lele.put(sourceLang,cs) ;
                    }else{
                        cs = new CounterSet() ;
                        cs.incr(targetLang) ;
                        lele.put(sourceLang,cs) ;
                    }
                }
            }
        }
        log.debug("LELE : "+lele.keySet().size()) ;
        log.debug("LEWS : "+lews.keySet().size()) ;
        log.debug("WSLE : "+wsle.keySet().size()) ;
        log.debug("WSWS : "+wsws.keySet().size()) ;
        for(String l : lele.keySet()){
            log.debug("\nNumber of Arcs LELE from "+l+" to : ") ;
            lele.get(l).logCounters(log);
        }
        for(String l : lews.keySet()){
            log.debug("\nNumber of Arcs LEWS from "+l+" to : ") ;
            lews.get(l).logCounters(log);
        }
        for(String l : wsle.keySet()){
            log.debug("\nNumber of Arcs WSLE from "+l+" to : ") ;
            wsle.get(l).logCounters(log);
        }
        for(String l : wsws.keySet()){
            log.debug("\nNumber of Arcs WSWS from "+l+" to : ") ;
            wsws.get(l).logCounters(log);
        }
    }

    public static void getGraphStatsVertices(SimpleDirectedGraph<String,DefaultEdge> g, Logger log){
        CounterSet vle = new CounterSet() ;
        CounterSet vws = new CounterSet() ;
        Map<String,String> ile = new HashMap<>() ;
        Map<String,String> iws = new HashMap<>() ;
        Map<String,String> ole = new HashMap<>() ;
        Map<String,String> ows = new HashMap<>() ;
        for(String v : g.vertexSet()){
            int din = g.inDegreeOf(v) ;
            int dout = g.outDegreeOf(v) ;
            String l = getLanguage(v) ;
            if(isSense(v)){
                vws.incr(l) ;
                String i = iws.get(l) ;
                if(i!=null){
                    iws.put(l,i+din+" ") ;
                }else{
                    iws.put(l,""+din+" ") ;
                }
                String o = ows.get(l) ;
                if(o!=null){
                    ows.put(l,o+dout+" ") ;
                }else{
                    ows.put(l,""+dout+" ") ;
                }
            }else{
                vle.incr(l) ;
                String i = ile.get(l) ;
                if(i!=null){
                    ile.put(l,i+din+" ") ;
                }else{
                    ile.put(l,""+din+" ") ;
                }
                String o = ole.get(l) ;
                if(o!=null){
                    ole.put(l,o+dout+" ") ;
                }else{
                    ole.put(l,""+dout+" ") ;
                }
            }
        }

        // Display
        String dispile = "\n" ;
        for(String l : ile.keySet()){
            dispile = dispile+l+" : "+ile.get(l)+"\n" ;
        }
        String dispiws = "\n" ;
        for(String l : iws.keySet()){
            dispiws = dispiws+l+" : "+iws.get(l)+"\n" ;
        }
        String dispole = "\n" ;
        for(String l : ole.keySet()){
            dispole = dispole+l+" : "+ole.get(l)+"\n" ;
        }
        String dispows = "\n" ;
        for(String l : ows.keySet()){
            dispows = dispows+l+" : "+ows.get(l)+"\n" ;
        }
        log.debug("\nNumber of LE vertices :") ;
        vle.logCounters(log);
        log.debug("\nNumber of WS vertices :") ;
        vws.logCounters(log);
        log.debug("\nRepartition of in degrees for LE vertices :"+dispile) ;
        log.debug("\nRepartition of in degrees for WS vertices :"+dispiws) ;
        log.debug("\nRepartition of out degrees for LE vertices :"+dispole) ;
        log.debug("\nRepartition of out degrees for WS vertices :"+dispows) ;
    }

    public static void main(String[] args) throws IOException {
        String fileTDB = args[0] ;
        String fileText = args[1] ;
        SimpleDirectedGraph<String,DefaultEdge> g = importGraphFromText(fileText) ;
        System.out.println("Imported") ;
        System.out.println("Graph has "+g.vertexSet().size()+" vertices and "+g.edgeSet().size()+" edges.") ;
    }
}
